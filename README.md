# control-tower-security

AWS security and customizations for Control Tower.


## Resources
* [CIS AWS Benchmarks](http://d1.awsstatic.com/whitepapers/compliance/AWS_CIS_Foundations_Benchmark.pdf)
* [Control Tower: User Guide](https://docs.aws.amazon.com/controltower/latest/userguide/controltower-ug.pdf)
* [Customizations for Control Tower](https://aws.amazon.com/solutions/implementations/customizations-for-aws-control-tower/)
* [Customizations for Control Tower: Developer Guide](https://s3.amazonaws.com/solutions-reference/customizations-for-aws-control-tower/latest/customizations-for-aws-control-tower-developer-guide.pdf)
* [Automated Response and Remediation with AWS Security Hub](https://aws.amazon.com/blogs/security/automated-response-and-remediation-with-aws-security-hub/)
* [Centralize Control Tower Security Hub](https://github.com/aws-samples/aws-control-tower-securityhub-enabler)
