DIST = dist
TEST = test
SCP = scp
SCRIPTS = scripts
LAMBDAS = lambdas
CFN = cloudformation
CUSTOM_CT = custom-control-tower-configuration

DIST_CFN := $(DIST)/$(CFN)
DIST_LAMBDAS := $(DIST)/$(LAMBDAS)
DIST_CUSTOM_CT := $(DIST)/$(CUSTOM_CT)
CUSTOM_CT_ARTIFACT = custom-control-tower-configuration.zip

# values should be lists of relative paths to files/directories separated by spaces
CUSTOM_CT_POLICIES := $(SCP)/*
CUSTOM_CT_TEMPLATES := $(CFN)/cis-benchmark-remediation $(CFN)/cicd-role.template
CUSTOM_CT_PARAMETERS := $(CFN)/cis-benchmark-remediation.params.json $(CFN)/cicd-role.params.json

S3_CP = aws s3 cp
CFN_SYNC_STACK = ./scripts/sync-cfn-stack.sh

# S3 variables to be overwritten by environment variables or makefile arguments
MASTER_ACCT_ID = 012345678910
REGION := $(if $(REGION),$(REGION),us-east-1)
S3_PREFIX := $(if $(S3_PREFIX),$(S3_PREFIX), aws)
S3_CFN_BUCKET := $(S3_PREFIX)-cloudformation-templates-$(REGION)
S3_LAMBDAS_BUCKET = $(S3_PREFIX)-lambdas-$(REGION)
S3_CTC_BUCKET = $(CUSTOM_CT)-$(MASTER_ACCT_ID)-$(REGION)


all: clean lint_bash lint_cfn lint_py test build

.PHONY: print_vars
print_vars:
	@echo AWS VARIABLES:
	@echo "- Region:            $(REGION)"
	@echo "- Master account id: $(MASTER_ACCT_ID)"
	@echo "- s3 prefix:         $(S3_PREFIX)"
	@echo "- s3 cfn bucket:     $(S3_CFN_BUCKET)"
	@echo "- s3 lambdas bucket: $(S3_LAMBDAS_BUCKET)"
	@echo "- s3 ctc bucket:     $(S3_CTC_BUCKET)"

# LINT #
#####################
.PHONY: lint_bash
lint_bash:
	@echo "Running shellchecker..."
	@shellcheck $(SCRIPTS)/*.sh

.PHONY: lint_cfn 
lint_cfn:
	@echo "Running cloudformation validator..."
	@cfn-lint $(CFN)/**/*.template

.PHONY: lint_py
lint_py:
	@echo "Running python linter..."
	@pylint $(LAMBDAS)

# TEST #
#####################
.PHONY: test
test:
	@echo "Running python unit tests..."
	@pytest $(LAMBDAS)/$(TEST)


# BUILD #
#####################
.PHONY: build_lambdas
build_lambdas:
	@echo "Packaging lambdas..."
	@mkdir -p $(DIST_LAMBDAS) || true
	@find $(LAMBDAS) -type d -mindepth 1 -maxdepth 1 -exec zip -r $(DIST)/{}.zip {} \;

.PHONY: build_ctc
build_ctc:
	@echo "Packaging custom Control Tower configurations..."
	@mkdir -p $(DIST_CUSTOM_CT)/templates $(DIST_CUSTOM_CT)/policies $(DIST_CUSTOM_CT)/parameters  || true
	@cp $(CUSTOM_CT_POLICIES) $(DIST_CUSTOM_CT)/policies
	@cp -R $(CUSTOM_CT_TEMPLATES) $(DIST_CUSTOM_CT)/templates
	@cp $(CUSTOM_CT_PARAMETERS) $(DIST_CUSTOM_CT)/parameters
	@cp ctc_manifest.yaml $(DIST_CUSTOM_CT)/manifest.yaml
	@cd $(DIST_CUSTOM_CT); zip -r ../$(CUSTOM_CT_ARTIFACT) .
	@rm -rf $(DIST_CUSTOM_CT)

.PHONY: build_cfn
build_cfn:
	@echo "Packaging cloudformation templates..."
	@mkdir -p $(DIST_CFN) || true
	@cp -R $(CFN)/* $(DIST_CFN)

.PHONY: build
build: clean build_lambdas build_ctconfig build_cfn


# DEPLOY #
#####################
.PHONY: deploy_lambdas
deploy_lambdas:
	# Deploy lambdas to s3 bucket
	echo "Uploading lambda functions to s3..."
	$(S3_CP) --recursive $(DIST_CFN) $(S3_CFN_BUCKET)/

.PHONY: deploy_cfn
deploy_cfn:
	# Deploy cloudformation templates to cloudformation buckets
	echo "Uploading cloudformation tempaltes to s3..."
	$(S3_CP) --recursive $(DIST_LAMBDAS) $(S3_LAMBDAS_BUCKET)/

# BEGIN CFN DEPLOY PARAMETERS #
S3CloudformationBucket := https://s3.amazonaws.com/$(S3_PREFIX)-cloudformation-templates-$(REGION)
S3CFN_URL_PARAM := ParameterKey=S3CloudformationBucket,ParameterValue=$(S3CloudformationBucket)

OrganizationId = o-0123456789
ORGANIZATION_ID_PARAM := ParameterKey=OrganizationId,ParameterValue=$(OrganizationId)
SecurityAccountId = 012345678910
SECURITY_ACCT_ID_PARAM := ParameterKey=SecurityAccountId,ParameterValue=$(SecurityAccountId)
LambdaSourceBucket = $(S3_LAMBDAS_BUCKET)
LAMBDA_BUCKET_PARAM := ParameterKey=S3SourceBucket,ParameterValue=$(LambdaSourceBucket)

MonitorEmail = monitoring@example.com
MONITOR_EMAIL_PARAM := ParameterKey=SetEmail,ParameterValue=$(MonitorEmail)
CloudTrailBucket = bucket
CLOUDTRAIL_BUCKET_PARAM := ParameterKey=PreExistingS3BucketCloudTrail,ParameterValue=$(CloudTrailBucket)
CloudTrailRoleName = role
CLOUDTRAIL_ROLE_PARAM := ParameterKey=NameCloudTrailRole,ParameterValue=$(CloudTrailRoleName)

CTC_PARAMS := $(S3CFN_URL_PARAM)
SECURITYHUB_PARAMS := $(ORGANIZATION_ID_PARAM) $(SECURITY_ACCT_ID_PARAM) $(LAMBDA_BUCKET_PARAM)
CIS_REMEDIATION_PARAMS := $(S3CFN_URL_PARAM) $(MONITOR_EMAIL_PARAM) $(CLOUDTRAIL_BUCKET_PARAM) $(CLOUDTRAIL_ROLE_PARAM)
# CFN DEPLOY PARAMETERS #

.PHONY: deploy_master_stacks
deploy_master_stacks:
	# Deploy infrastructure cloudformation stacks for the master account
	echo "Deploying cloudformation stacks to master account..."
	# Sync ctc pipeline
	$(CFN_SYNC_STACK) NTTCustomControlTowerPipeline $(REGION) --template-url $(S3CloudformationBucket)/ctc-pipeline/index.template --capabilities CAPABILITY_NAMED_IAM
	# Sync security hub enabler
	$(CFN_SYNC_STACK) NTTSecurityHubEnabler $(REGION) --template-url $(S3CloudformationBucket)/securityhub-enabler.template --parameters $(SECURITYHUB_PARAMS) --capabilities CAPABILITY_NAMED_IAM
	# Sync cis benchmark remediation
	$(CFN_SYNC_STACK) NTTCISBenchmarkRemediations $(REGION) --template-url $(S3CloudformationBucket)/cis-benchmark-remediation/index.template --parameters $(CIS_REMEDIATION_PARAMS) --capabilities CAPABILITY_NAMED_IAM
	# Sync vpc
	$(CFN_SYNC_STACK) NTTBasicVPC $(REGION) --template-url $(S3CloudformationBucket)/vpn-vpc-private.template

.PHONY: deploy_ctc
deploy_ctc:
	# Deploy the control tower customizations
	echo "Deploying custom control tower configurations..."
	$(S3_CP) $(DIST)/$(CUSTOM_CT_ARTIFACT) $(S3_CTC_BUCKET)/$(CUSTOM_CT_ARTIFACT)

.PHONY: deploy
deploy: deploy_lambdas deploy_cfn deploy_master_stacks deploy_ctc


# CLEAN #
#####################
.PHONY: clean
clean:
	@echo Cleaning...
	@rm -rf $(DIST)