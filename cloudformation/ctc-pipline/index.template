AWSTemplateFormatVersion: '2010-09-09'
Description: '(SO0089) - customizations-for-aws-control-tower Solution. Version: v1.1.0'

Parameters:
  S3CloudformationBucket:
    Type: String
    Description: S3 bucket http URL that contains cloudformation templates
    Default: https://s3.amazonaws.com/aws-cloudformation-templates-us-east-1
  EnableApprovalStage:
    Description: Do you want to add a manual approval stage to the Custom Control Tower Configuration Pipeline?
    AllowedValues:
      - 'Yes'
      - 'No'
    Default: 'No'
    Type: String
  ApprovalEmail:
    Description: (Not required if Pipeline Approval Stage = 'No') Email for notifying that the CustomControlTower pipeline is waiting for an Approval
    Type: String

Resources: 
  ControlTowerCustomizationsStateMachine: 
    Type: AWS::CloudFormation::Stack
    Properties: 
      TemplateURL: !Sub "${S3CloudformationBucket}/ctc-pipeline/state-machine.template"
  ControlTowerCustomizationsBuildPipeline: 
    Type: AWS::CloudFormation::Stack
    Properties: 
      TemplateURL: !Sub "${S3CloudformationBucket}/ctc-pipeline/build-pipeline.template"
      Parameters: 
        PipelineApprovalStage: !Ref EnableApprovalStage
        PipelineApprovalEmail: !Ref ApprovalEmail
        StateMachineLambdaRole: !GetAtt ControlTowerCustomizationsStateMachine.Outputs.StateMachineLambdaRoleArn
        ServiceControlPolicyMachine: !GetAtt ControlTowerCustomizationsStateMachine.Outputs.SCPMachineArn
        StackSetStateMachine: !GetAtt ControlTowerCustomizationsStateMachine.Outputs.CFNStateMachineArn
