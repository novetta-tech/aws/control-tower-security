#!/usr/bin/env bash

usage="Usage: $(basename "$0") path [aws-cli-opts]

where:
  path = path to cloudformation file or directory relative to the root of the repo. Do not include \"./\"
"

if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ "$1" == "help" ] || [ "$1" == "usage" ] ; then
  echo "$usage"
  exit 5
fi

if [ -z "$1" ] ; then
  echo "$usage"
  exit 5
fi

shopt -s failglob
set -eu -o pipefail

echo "Checking if argument is a file or directory ..."

if [[ -f "$1" ]] ; then

  echo -e "\nFile argument detected..."
  if [[ $1 == *.template ]] || [[ $1 == *.yaml ]] ; then

    aws cloudformation validate-template \
      --template-body "file://${1}" "${@:2}"

  else

    echo -e "\nFile must have the \".cfn\"  or \".yaml\" extension."

  fi

elif [[ -d "$1" ]] ; then

  echo -e "\nDirectory argument detected..."
  find "./${1}" \( -name "*.template" \) -print | \
    cut -c3- | \
    xargs -I{} -t \
    aws cloudformation validate-template \
      --template-body file://{} "${@:2}"

else

  echo -e "\nPath provided is invalid or does not exist."
  exit 1

fi